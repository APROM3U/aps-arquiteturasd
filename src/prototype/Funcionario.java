package prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Suponha que temos um objeto que carrega dados do banco de dados. Agora precisamos modificar esses dados
 * em nosso programa várias vezes, portanto, não é uma boa ideia criar a um novo objeto e carregar todos os dados
 * novamente do DB. A melhor abordagem seria clonar o objeto existente em um novo objeto e, em seguida, fazer a
 * manipulação de dados. O padrão de projeto de protótipo determina que o objeto que você está copiando deve
 * fornecer o recurso de cópia. Não deve ser feito por nenhuma outra classe.
 * No entanto, usar uma cópia rasa ou profunda das propriedades do objeto depende dos requisitos e é uma decisão de
 * design.
 */
public class Funcionario implements Cloneable {
    private List<String> listaColab;

    public Funcionario() {
        listaColab = new ArrayList<String>();
    }

    public Funcionario(List<String> list) {
        this.listaColab = list;
    }

    public void loadData() {
        // Le todos os colaboradores do DB e copia para uma lista
        listaColab.add("Joao");
        listaColab.add("Ana");
        listaColab.add("Pedro");
        listaColab.add("Maria");
        listaColab.add("Jose");
    }

    public List<String> getListaColab() {
        return listaColab;
    }

    /**
     * Observe que o método clone é substituído para fornecer uma cópia profunda da lista de funcionários.
     * Se a clonagem do objeto não foi fornecida, teremos que fazer uma chamada ao banco de dados para buscar a
     * lista de colaboradores todas as vezes. Também teriamos todas as manipulações que consumiriam recursos e tempo.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        List<String> listAux = new ArrayList<String>();
        for (String x : this.getListaColab()) {
            listAux.add(x);
        }
        return new Funcionario(listAux);
    }
}
