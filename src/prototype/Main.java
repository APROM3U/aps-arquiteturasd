package prototype;

import java.util.List;

/** Design Patterns - Prototype
 * é usado quando a criação do objeto é um assunto caro e requer muito tempo e recursos e você
 * já possui um objeto semelhante. O padrão de protótipo fornece um mecanismo para copiar o objeto
 * original para um novo objeto e modificá-lo de acordo com nossas necessidades.
 * O padrão de design do protótipo usa clonagem java para copiar o objeto.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Funcionario func = new Funcionario();
        func.loadData();

        // Utiliza o metodo clone para obter os dados do objeto funcionario.
        Funcionario colab = (Funcionario) func.clone();
        Funcionario colab2 = (Funcionario) func.clone();

        List<String> lista1 = colab.getListaColab();
        lista1.add("Jacqueline");
        List<String> lista2 = colab2.getListaColab();
        lista2.remove("Maria");

        System.out.println("DB - Lista de colaboradores: " + func.getListaColab());
        System.out.println("Lista 1: " + lista1);
        System.out.println("Lista 2: " + lista2);
    }
}