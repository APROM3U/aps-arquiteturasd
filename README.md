Alexandre Prado Romeu - RA 125111375642
Arquitetura de Sistemas Distribuidos - Eng. Computação

# Design Patterns

Os padrões de projeto (design patterns) são como plantas pré-projetadas de uma construção, que você pode alterar para se adequar melhor na resolução de um problema recorrente em seu código. O que diferencia os padrões de projeto das funções e bibliotecas é que você não pode simplesmente copiá-los direto para seu programa, já que eles não são um pedaço de código, mas sim um conceito que serve como uma solução.

Assim, para se implementar um padrão de projeto você deve seguir o conceito dos padrões escolhidos (dentre todos os existentes) e ajustá-lo ao problema que deseja resolver. Isso dependerá das características do projeto. Dessa forma, fazemos uma implementação que se encaixe exatamente à demanda da nossa aplicação.

No arquivo PDF tem uma breve explicação de cada um dos três principais padrões de projeto definidos pelo livro “Design Patterns: Elements of Reusable Object-Oriented Software” de 1994, escrito por GOF (Gang of Four: Rich Gamma, Richard Helm, Ralph Johnson e John Vlissides) são os padrões criacionais, estruturais e comportamentais. Esses padrões foram divididos e agrupados de acordo com a natureza do problema que eles solucionam.

### Design Patterns - Prototype
É utilizado quando a criação do objeto é um assunto caro e requer muito tempo e recursos e você já possui um objeto semelhante. O padrão de protótipo fornece um mecanismo para copiar o objeto original para um novo objeto e modificá-lo de acordo com nossas necessidades. O padrão de design do protótipo usa clonagem java para copiar o objeto.

Suponha que temos um objeto que carrega dados do banco de dados. Agora precisamos modificar esses dados em nosso programa várias vezes, portanto, não é uma boa ideia criar a um novo objeto e carregar todos os dados novamente do DB. A melhor abordagem seria clonar o objeto existente em um novo objeto e, em seguida, fazer a manipulação de dados.

NOTA: O padrão de projeto de protótipo determina que o objeto que você está copiando deve fornecer o recurso de cópia. Não deve ser feito por nenhuma outra classe.
No entanto, usar uma cópia rasa ou profunda das propriedades do objeto depende dos requisitos e é uma decisão de
design.

Segue um código exemplificando a busca e manipulação de uma lista de colaboradores.
